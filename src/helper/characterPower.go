package helpers

func Define(code int, power float64) int {
	if code == 1 {
		power = power * 1.5
	}
	if code == 2 {
		power = (power * (1.1)) + 2
	}
	if code == 3 {
		if power < 20 {
			power = power * (200 / 100)
		} else {
			power = power * (300 / 100)
		}
	}

	return int(power)
}
