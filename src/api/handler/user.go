package handler

import (
	"fmt"
	"net/http"
	"whim/src/domain"
	helpers "whim/src/helper"
	middlewares "whim/src/middleware"
	Error "whim/src/pkg/error"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

func (a *AppHandler) Login(c *gin.Context) {
	errorParams := map[string]interface{}{}
	statusCode := 200
	var body domain.Login
	err := c.ShouldBindJSON(&body)
	if err != nil {
		statusCode = 406
		Error.Error(err)
		errorParams["meta"] = map[string]interface{}{
			"status":  statusCode,
			"message": err.Error(),
		}
		errorParams["code"] = statusCode
		c.JSON(statusCode, helpers.OutputAPIResponseWithPayload(errorParams))
		return
	}

	data, err := a.Entity.Login(c, body)
	if err != nil {
		statusCode = 400
		Error.Error(err)
		errorParams["meta"] = map[string]interface{}{
			"status":  statusCode,
			"message": err.Error(),
		}
		errorParams["code"] = statusCode
		c.JSON(statusCode, helpers.OutputAPIResponseWithPayload(errorParams))
		return
	}

	// Compare Password
	err = bcrypt.CompareHashAndPassword([]byte(data.Password), []byte(body.Password))
	if err != nil {
		statusCode = 400
		Error.Error(err)
		errorParams["meta"] = map[string]interface{}{
			"status":  statusCode,
			"message": err.Error(),
		}
		errorParams["code"] = statusCode
		c.JSON(statusCode, helpers.OutputAPIResponseWithPayload(errorParams))
		return
	}

	token, err := middlewares.CreateToken(data)
	if err != nil {
		statusCode = 400
		Error.Error(err)
		errorParams["meta"] = map[string]interface{}{
			"status":  statusCode,
			"message": err.Error(),
		}
		errorParams["code"] = statusCode
		c.JSON(statusCode, helpers.OutputAPIResponseWithPayload(errorParams))
		return
	}

	data.Password = ""

	params := map[string]interface{}{
		"meta": map[string]interface{}{
			"token": token,
		},
		"payload": data,
	}
	c.JSON(statusCode, helpers.OutputAPIResponseWithPayload(params))
}

func (a *AppHandler) GetUserById(c *gin.Context) {
	errorParams := map[string]interface{}{}
	User := middlewares.GetUserCustom(c)
	fmt.Println("user =>>", User)
	res, err := a.Entity.GetUserById(c, User["uid"].(float64))
	if err != nil {
		Error.Error(err)
		statusCode := http.StatusBadRequest
		errorParams["meta"] = map[string]interface{}{
			"status":  statusCode,
			"message": err.Error(),
		}
		errorParams["code"] = statusCode
		c.JSON(statusCode, helpers.OutputAPIResponseWithPayload(errorParams))
		return
	}

	params := map[string]interface{}{
		"meta":    "success",
		"payload": res,
	}
	c.JSON(http.StatusOK, helpers.OutputAPIResponseWithPayload(params))
}

func (a *AppHandler) CreateUser(c *gin.Context) {
	errorParams := map[string]interface{}{}
	statusCode := 200
	var body domain.SetUser
	err := c.ShouldBindJSON(&body)
	if err != nil {
		statusCode = 406
		errorParams["meta"] = map[string]interface{}{
			"status":  statusCode,
			"message": err.Error(),
		}
		errorParams["code"] = statusCode
		c.JSON(statusCode, helpers.OutputAPIResponseWithPayload(errorParams))
		return
	}

	bytes, err := bcrypt.GenerateFromPassword([]byte(body.Password), 14)
	body.Password = string(bytes)

	err = a.Entity.CreateUser(c, body)
	if err != nil {
		statusCode = 400
		errorParams["meta"] = map[string]interface{}{
			"status":  statusCode,
			"message": err.Error(),
		}
		errorParams["code"] = statusCode
		c.JSON(statusCode, helpers.OutputAPIResponseWithPayload(errorParams))
		return
	}

	params := map[string]interface{}{
		"meta":    "success",
		"payload": body,
	}
	c.JSON(statusCode, helpers.OutputAPIResponseWithPayload(params))
}
