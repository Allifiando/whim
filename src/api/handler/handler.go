package handler

import (
	"whim/src/domain"
	helpers "whim/src/helper"
	"whim/src/middleware"

	"github.com/gin-gonic/gin"
)

type AppHandler struct {
	Entity domain.Entity
}

func InitHandler(r *gin.RouterGroup, e domain.Entity) {
	handler := &AppHandler{
		Entity: e,
	}

	r.GET("/", handler.Home)

	users := r.Group("/user")
	{
		users.POST("/login", handler.Login)
		users.GET("/info", middleware.Auth(handler.Entity), handler.GetUserById)
		users.POST("/register", handler.CreateUser)
	}

	characters := r.Group("/character")
	{
		characters.GET("/", middleware.Auth(handler.Entity), handler.GetListCharacter)
		characters.POST("/", middleware.Auth(handler.Entity), handler.CreateCharacter)
		characters.PUT("/id/:id", middleware.Auth(handler.Entity), handler.UpdateCharacter)
	}
}

func (a *AppHandler) Home(c *gin.Context) {
	params := map[string]interface{}{
		"payload": gin.H{"message": "OK", "version": "1"},
		"meta":    gin.H{"message": "OK"},
	}
	c.JSON(200, helpers.OutputAPIResponseWithPayload(params))
}
