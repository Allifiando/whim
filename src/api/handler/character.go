package handler

import (
	"net/http"
	"strconv"
	"whim/src/domain"
	helpers "whim/src/helper"

	"github.com/gin-gonic/gin"
)

func (a *AppHandler) GetListCharacter(c *gin.Context) {
	errorParams := map[string]interface{}{}
	limit, _ := strconv.Atoi(c.Query("limit"))
	page, _ := strconv.Atoi(c.Query("page"))
	search := c.Query("search")

	if page == 0 {
		page = helpers.DefaultPage
	}

	limit, offset := helpers.PaginationPageOffset(page, limit)
	data, count, err := a.Entity.GetListCharacter(c, offset, limit, search)
	if err != nil {
		statusCode := http.StatusBadRequest
		errorParams["meta"] = map[string]interface{}{
			"status":  http.StatusBadRequest,
			"message": err.Error(),
		}
		errorParams["code"] = statusCode
		c.JSON(statusCode, helpers.OutputAPIResponseWithPayload(errorParams))
		return
	}

	pagination := helpers.PaginationRes(page, count, limit)
	params := map[string]interface{}{
		"payload": data,
		"meta":    pagination,
	}
	c.JSON(http.StatusOK, helpers.OutputAPIResponseWithPayload(params))
}

func (a *AppHandler) CreateCharacter(c *gin.Context) {
	errorParams := map[string]interface{}{}
	statusCode := 200
	var body domain.SetCharacter
	err := c.ShouldBindJSON(&body)
	if err != nil {
		statusCode = 406
		errorParams["meta"] = map[string]interface{}{
			"status":  statusCode,
			"message": err.Error(),
		}
		errorParams["code"] = statusCode
		c.JSON(statusCode, helpers.OutputAPIResponseWithPayload(errorParams))
		return
	}

	err = a.Entity.CreateCharacter(c, body)
	if err != nil {
		statusCode = 400
		errorParams["meta"] = map[string]interface{}{
			"status":  statusCode,
			"message": err.Error(),
		}
		errorParams["code"] = statusCode
		c.JSON(statusCode, helpers.OutputAPIResponseWithPayload(errorParams))
		return
	}

	params := map[string]interface{}{
		"meta":    "success",
		"payload": body,
	}
	c.JSON(statusCode, helpers.OutputAPIResponseWithPayload(params))
}

func (a *AppHandler) UpdateCharacter(c *gin.Context) {
	errorParams := map[string]interface{}{}
	statusCode := 200
	param := c.Param("id")
	id, _ := strconv.Atoi(param)

	var body domain.SetCharacter
	err := c.ShouldBindJSON(&body)
	if err != nil {
		statusCode = 406
		errorParams["meta"] = map[string]interface{}{
			"status":  statusCode,
			"message": err.Error(),
		}
		errorParams["code"] = statusCode
		c.JSON(statusCode, helpers.OutputAPIResponseWithPayload(errorParams))
		return
	}

	err = a.Entity.UpdateCharacter(c, body, id)
	if err != nil {
		statusCode = 400
		errorParams["meta"] = map[string]interface{}{
			"status":  statusCode,
			"message": err.Error(),
		}
		errorParams["code"] = statusCode
		c.JSON(statusCode, helpers.OutputAPIResponseWithPayload(errorParams))
		return
	}

	params := map[string]interface{}{
		"meta":    "success",
		"payload": body,
	}
	c.JSON(statusCode, helpers.OutputAPIResponseWithPayload(params))
}
