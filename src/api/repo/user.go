package repo

import (
	"context"

	"whim/src/domain"
	Error "whim/src/pkg/error"
)

func (m *Repo) Login(ctx context.Context, b domain.Login) (data domain.UserLogin, err error) {
	query := `SELECT u.id, u.email, u.password, u.role_id 	
	FROM users u 	
	where u.email = ?`
	err = m.Conn.QueryRow(query, b.Email).Scan(&data.ID, &data.Email, &data.Password,
		&data.RoleId)

	if err != nil {
		Error.Error(err)
		return data, err
	}
	return
}

func (m *Repo) GetUserById(ctx context.Context, uid float64) (res domain.User, err error) {
	query := `SELECT id, email FROM users WHERE id = ?`
	err = m.Conn.QueryRow(query, uid).Scan(&res.ID, &res.Email)
	if err != nil {
		return
	}
	return
}

func (m *Repo) CreateUser(ctx context.Context, b domain.SetUser) (err error) {
	query := `INSERT INTO users(email,name,password,role_id) 
	VALUES(?,?,?,?)`
	stmt, err := m.db.PrepareContext(ctx, query)
	if err != nil {
		return
	}
	_, err = stmt.ExecContext(ctx,
		b.Email,
		b.Name,
		b.Password,
		b.Role,
	)
	if err != nil {
		return
	}
	return
}
