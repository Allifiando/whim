package repo

import (
	"context"
	"database/sql"
	"whim/src/domain"
)

func (m *Repo) GetListCharacter(ctx context.Context, offset, limit int, search string) (res []domain.GetCharacter, count int, err error) {
	query := `SELECT c.id, c.name, cc.id, cc.name, c.power
	FROM characters c
	JOIN character_code cc ON c.character_code = cc.id
	WHERE c.is_active = 1`

	if search != "" {
		query += ` AND (c.name LIKE ?)`
	}

	query += ` LIMIT ? OFFSET ?`

	var rows *sql.Rows = nil
	if search != "" {
		rows, err = m.Conn.QueryContext(ctx, query, "%"+search+"%", limit, offset)
	} else {
		rows, err = m.Conn.QueryContext(ctx, query, limit, offset)
	}

	if err != nil {
		return
	}
	defer rows.Close()
	for rows.Next() {
		d := domain.GetCharacter{}
		err = rows.Scan(&d.ID, &d.Name, &d.CharacterCode, &d.Character, &d.Power)
		if err != nil {
			return
		}
		res = append(res, d)
	}

	// Query row count
	query = `SELECT COUNT(*)
	FROM characters 	
	WHERE is_active = 1`
	if search != "" {
		query += ` AND (name LIKE ?)`
	}

	if search != "" {
		err = m.Conn.QueryRow(query, "%"+search+"%").Scan(&count)
	} else {
		err = m.Conn.QueryRow(query).Scan(&count)
	}
	if err != nil {
		return
	}
	return
}

func (m *Repo) CreateCharacter(ctx context.Context, b domain.SetCharacter) (err error) {
	query := `INSERT INTO characters(character_code,name,power) 
	VALUES(?,?,?)`
	stmt, err := m.db.PrepareContext(ctx, query)
	if err != nil {
		return
	}
	_, err = stmt.ExecContext(ctx,
		b.Character,
		b.Name,
		b.Power,
	)
	if err != nil {
		return
	}
	return
}

func (m *Repo) GetCharacter(ctx context.Context, uid float64) (res domain.GetCharacter, err error) {
	query := `SELECT id, character_code, name, power FROM characters WHERE id = ? and is_active = 1`
	err = m.Conn.QueryRow(query, uid).Scan(&res.ID, &res.CharacterCode, &res.Name, &res.Power)
	if err != nil {
		return
	}
	return
}

func (m *Repo) UpdateCharacter(ctx context.Context, b domain.SetCharacter, id int) (err error) {
	query := `UPDATE characters 	
	SET name = ?, power = ?
	WHERE id = ?`

	stmt, err := m.db.PrepareContext(ctx, query)
	if err != nil {
		return
	}

	res, err := stmt.ExecContext(ctx, b.Name, b.Power, id)
	if err != nil {
		return
	}
	affect, err := res.RowsAffected()
	if err != nil {
		return
	}
	if affect != 1 {
		// no affected data
		return
	}
	return
}
