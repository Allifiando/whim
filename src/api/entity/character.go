package entity

import (
	"context"
	"errors"
	"whim/src/domain"
	helpers "whim/src/helper"
)

func (a *Entity) GetListCharacter(c context.Context, offset, limit int, search string) (res []domain.GetCharacter, count int, err error) {
	ctx, cancel := context.WithTimeout(c, a.timeout)
	defer cancel()

	res, count, err = a.repo.GetListCharacter(ctx, offset, limit, search)

	if len(res) > 0 {
		for i := 0; i < len(res); i++ {
			res[i].Value = helpers.Define(res[i].CharacterCode, res[i].Power)
		}
	}

	if err != nil {
		err = errors.New("gagal get data user")
		return res, count, err
	}
	return
}

func (a *Entity) CreateCharacter(c context.Context, b domain.SetCharacter) (err error) {
	ctx, cancel := context.WithTimeout(c, a.timeout)
	defer cancel()

	err = a.repo.CreateCharacter(ctx, b)
	if err != nil {
		err = errors.New("gagal create user")
		return err
	}
	return
}

func (a *Entity) UpdateCharacter(c context.Context, b domain.SetCharacter, id int) (err error) {
	ctx, cancel := context.WithTimeout(c, a.timeout)
	defer cancel()

	err = a.repo.UpdateCharacter(ctx, b, id)
	if err != nil {
		err = errors.New("gagal update user")
		return err
	}
	return
}
