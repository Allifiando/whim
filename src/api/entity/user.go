package entity

import (
	"context"
	"errors"

	"whim/src/domain"
)

func (a *Entity) Login(c context.Context, b domain.Login) (res domain.UserLogin, err error) {
	ctx, cancel := context.WithTimeout(c, a.timeout)
	defer cancel()

	res, err = a.repo.Login(ctx, b)
	if err != nil {
		err = errors.New("gagal login")
		return res, err
	}
	return
}

func (a *Entity) GetUserById(c context.Context, uid float64) (res domain.User, err error) {
	ctx, cancel := context.WithTimeout(c, a.timeout)
	defer cancel()

	res, err = a.repo.GetUserById(ctx, uid)
	if err != nil {
		err = errors.New("gagal get data")
		return res, err
	}
	return
}

func (a *Entity) CreateUser(c context.Context, b domain.SetUser) (err error) {
	ctx, cancel := context.WithTimeout(c, a.timeout)
	defer cancel()

	err = a.repo.CreateUser(ctx, b)
	if err != nil {
		err = errors.New("gagal create user")
		return err
	}
	return
}
