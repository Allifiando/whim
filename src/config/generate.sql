CREATE TABLE users (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(50) NOT NULL,
    name VARCHAR(30) NOT NULL,
    password VARCHAR(100) NOT NULL,
    is_active bool default 1,
    role_id int 
)

CREATE TABLE roles (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(30) NOT NULL,
    is_active bool default 1
)

CREATE TABLE character_code (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    dsc VARCHAR(100),
    is_active bool default 1
)

CREATE TABLE characters (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    character_code int,
    name VARCHAR(50) NOT NULL,
    power int not null,
    is_active bool default 1
)