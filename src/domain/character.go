package domain

type GetCharacter struct {
	ID            int     `json:"id"`
	Name          string  `json:"name"`
	CharacterCode int     `json:"character_code"`
	Character     string  `json:"character"`
	Power         float64 `json:"power"`
	Value         int     `json:"value"`
}

type SetCharacter struct {
	Name      string `json:"name"`
	Character int    `json:"character"`
	Power     int    `json:"power"`
}
