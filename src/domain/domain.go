package domain

import "context"

type Entity interface {
	Login(ctx context.Context, b Login) (res UserLogin, err error)
	GetUserById(ctx context.Context, uid float64) (res User, err error)
	CreateUser(ctx context.Context, b SetUser) (err error)

	GetListCharacter(ctx context.Context, offset, limit int, search string) (res []GetCharacter, count int, err error)
	CreateCharacter(ctx context.Context, b SetCharacter) (err error)
	UpdateCharacter(ctx context.Context, b SetCharacter, id int) (err error)
}
type Repo interface {
	Login(ctx context.Context, b Login) (res UserLogin, err error)
	GetUserById(ctx context.Context, uid float64) (res User, err error)
	CreateUser(ctx context.Context, b SetUser) (err error)

	GetListCharacter(ctx context.Context, offset, limit int, search string) (res []GetCharacter, count int, err error)
	CreateCharacter(ctx context.Context, b SetCharacter) (err error)
	UpdateCharacter(ctx context.Context, b SetCharacter, id int) (err error)
}
