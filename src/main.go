package main

import (
	"log"
	"os"
	"strconv"
	"time"

	"whim/src/config"

	_entity "whim/src/api/entity"
	_handler "whim/src/api/handler"
	_repo "whim/src/api/repo"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal(err)
		log.Fatal("Error getting env")
	}

	port := os.Getenv("PORT")
	if port == "" {
		port = "1234"
	}

	timeout := os.Getenv("TIMEOUT")
	if timeout == "" {
		timeout = "2"
	}

	i, _ := strconv.Atoi(timeout)
	timeoutContext := time.Duration(i) * time.Second

	config.InitSql()
	db := config.GetSqlDB()

	repo := _repo.InitRepo(db)
	entity := _entity.InitEntity(repo, timeoutContext)

	r := gin.Default()
	// r.Use(middleware.CORS())
	api := r.Group("/")

	_handler.InitHandler(api, entity)
	r.Run(":" + port)

}
